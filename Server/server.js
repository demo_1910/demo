const express = require('express')
const cors = require('cors')
const app = express()
app.use(express.json())
app.use(cors())

app.use('/user', require('./Routes/user'))
app.use('/car', require('./Routes/car'))

app.listen(4000, '0.0.0.0', () =>
  console.log('Server is Started on port 4000 ...')
)
