```SQL
    -- carTB
        create table carTB(
            id int primary key auto_increment,
            name varchar(40),
            model varchar(40),
            price decimal(10,2)
        );
    -- userTB
        create table userTB(
            empid int primary key auto_increment,
            companyname varchar(60),
            email varchar(100),
            password varchar(50)
        );

```
