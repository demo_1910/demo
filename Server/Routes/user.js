const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/signIn', (request, response) => {
  const { email, password } = request.body

  const statement = `
    SELECT empid from userTB WHERE email = ? AND password = ?
  `
  db.pool.query(statement, [email, password], (error, data) => {
    if (!error && data.length === 0)
      response.send(utils.createResult('user does not exists'))
    else {
      response.send(utils.createResult(error, data))
    }
  })
})

module.exports = router
