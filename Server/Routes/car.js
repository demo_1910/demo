const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/addCar', (request, response) => {
  const { name, model, price } = request.body

  const statement = `
    INSERT INTO 
        carTB(name, model, price )
    VALUES( ?, ?, ? )
  `
  db.pool.query(statement, [name, model, price], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.get('/allCars', (request, response) => {
  const statement = `
        SELECT id, name, model, price FROM carTB
    `
  db.pool.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

router.delete('/deleteCar/:id', (request, response) => {
  const statement = `
        DELETE FROM carTB WHERE id = ?
    `
  db.pool.query(statement, [request.params.id], (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

module.exports = router
